package com.simplilern.lockme.application;



import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.simplilern.lockme.model.users;
import com.simplilern.lockme.model.userscredential;

public class sample {
	
//	input data
	private static Scanner keyboard;
	private static Scanner input;
	private static Scanner lockerInput;

//	output data
	private static PrintWriter output;
	private static PrintWriter lockerOutput;
	
//	model to store data
	private static users users;
	private static userscredential userscredential;
	
	public static void main(String[] args) {
		
		initApp();
		welcomeScreen();
		signInOptions();
		
	}
	public static void signInOptions() {
		
		System.out.println("1. Registration");
		System.out.println("2. Login");
		int option = keyboard.nextInt();
		switch(option) {
		case 1:
			registerUser();
			break;
		case 2:
			loginUser();
			break;
		default :
			System.out.println("Please select 1 or 2");
			break;
		}
		keyboard.close();
		input.close();
		
	}
	
public static void lockerOptions(String inpUsername) {
		
		System.out.println("1. Fetch all stored credentials");
		System.out.println("2. Store credentials");
		int option = keyboard.nextInt();
		switch(option) {
		case 1:
			fetchCredentials(inpUsername);
			break;
		case 2:
			storeCredentials(inpUsername);
			break;
		default :
			System.out.println("Please select 1 or 2");
			break;
		}
		keyboard.close();
		lockerInput.close();
		
	}
	
	public static void registerUser() {
		System.out.println("=========================");
		System.out.println("*                       *");
		System.out.println(" WELCOME To REGISTRATION ");
		System.out.println("*                       *");
		System.out.println("*                       *");
		System.out.println("=========================");
		
		System.out.println("Enter Username :");
		String username = keyboard.next();
		users.setUsername(username);
		
		System.out.println("Enter Password");
		String password = keyboard.next();
		users.setPassword(password);
		
		output.println(users.getUsername());
		output.println(users.getPassword());
		
		System.out.println("REGISTRATION SUCCESSFUL");
		
		output.close();
		
	}
	public static void loginUser() {
		System.out.println("=========================");
		System.out.println("*                       *");
		System.out.println("  WELCOME To Login Page  ");
		System.out.println("*                       *");
		System.out.println("*                       *");
		System.out.println("=========================");
		
		System.out.println("Enter Username :");
		String inpUsername = keyboard.next();
		boolean found = false;
		while(input.hasNext() && !found) {
			if(input.next().equals(inpUsername)) {
				System.out.println("Enter Password :");
				String inpPassword = keyboard.next();
				if(input.next().equals(inpPassword)) {
					System.out.println("LOGIN SUCCESSFUL");
				found = true;
				lockerOptions(inpUsername);
				break;
				}
			}
			
		}
		if(!found) {
			System.out.println("User Not Found : Login Failed - 404");
		}
	}
	
	
	public static  void welcomeScreen() {
			System.out.println("=========================");
			System.out.println("*                       *");
			System.out.println(" Welcome To LockedMe.com ");
			System.out.println("  Your Own Digi Lockerr  ");
			System.out.println("*                       *");
			System.out.println("=========================");
	}
//	Store credentials
	public static void storeCredentials(String loggedInuser) {
		System.out.println("=========================");
		System.out.println("*                       *");
		System.out.println(" Welcome To LockedMe.com ");
		System.out.println("  Store your creds here  ");
		System.out.println("*                       *");
		System.out.println("=========================");
		
		userscredential.setLoggedInusers(loggedInuser);
		
		System.out.println("Enter Site Name :");
		String siteName = keyboard.next();
		//userscredential.setSiteName(siteName);
		userscredential.setSiteName(siteName);
		
		
		System.out.println("Enter Username :");
		String username = keyboard.next();
		userscredential.setUserName(username);
		
		System.out.println("Enter Password");
		String password = keyboard.next();
		userscredential.setPassWord(password);
		
		lockerOutput.println(userscredential.getLoggedUser());
		lockerOutput.println(userscredential.getSiteName());
		lockerOutput.println(userscredential.getUserName());
		lockerOutput.println(userscredential.getPassWord());
		
		System.out.println("ALL YOUR CREDS ARE STORED SUCCESSFULLY");
		
		lockerOutput.close();
	}
//	Fetch credentials
	public static void fetchCredentials(String inpUsername) {
		System.out.println("=========================");
		System.out.println("*                       *");
		System.out.println("   Your Digital Locker   ");
		System.out.println("*                       *");
		System.out.println("=========================");
		
		while(lockerInput.hasNext()) {
			if(lockerInput.next().equals(inpUsername)) {
				System.out.println("Site Name: "+lockerInput.next());
				System.out.println("User Name: "+lockerInput.next());
				System.out.println("Password : "+lockerInput.next());
			}
			
		}
	}
	
	
	public static void initApp() {
			
			File dbFile = new File("database.txt");
			File lockerFile = new File("lockerfile.txt");
			
			try {
//				read data from DB file
				input = new Scanner(dbFile);
				
//				read data from locker file
				lockerInput = new Scanner(lockerFile);
				
//				read data from keyboard
				keyboard = new Scanner(System.in);
				
//				output
				output = new PrintWriter(new FileWriter(dbFile,true));
				lockerOutput = new PrintWriter(new FileWriter (lockerFile,true));
				
				users = new users();
				userscredential = new userscredential();
				
				
				
			} catch (IOException e) {
				System.out.println("404: File Not Found");
			}
			
		}

}
